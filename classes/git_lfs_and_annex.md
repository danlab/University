footer: GitLab University | Nov 12, 2015

# Big files in git

---

> Don't put big files in git
-- everyone

---

> put everything in git
-- GitLab & friends

---

#### Solution #1

## Git Annex

---

# pro annex

* it works
* it works with gitlab

---

# con annex

- little support in the industry
- barely used
- hard to use
- hard to support
- Windows support is so-so

---

#### Solution #2

## Git Large File Storage

---

# Pro lfs

- Backed by all major players
- Easy to use
- Very serious, large scale effort
- Works on all platforms

---

# Con lfs

- very new
- still barely used (this will change)

---

## Alternatives?

---

- git-fat
- git-media
- git-bigstore
- git-sym

---

> Git LFS is the first usable option

---

# [fit] Questions?

<!-- # [fit] ⌘+C ⌘+V = :v: -->
