---
title: Trainings
----------------

## What are GitLab Trainings

This section hosts all training material used for our [training sessions](https://about.gitlab.com/training/). 

---

+ [End User Training](/training/end-user)
+ [End User Training Slides](http://45.55.216.78:8000/#/)