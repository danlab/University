# GitLab University Documents

This repository contains documents related to GitLab University.

For most information, see [the website](https://university.gitlab.com/).

## Viewing Slides

Use [Deckset](http://www.decksetapp.com/) to view the slides in a nice way.
